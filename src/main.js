// This is the main.js file. Import global CSS and scripts here.
// The Client API can be used here. Learn more: gridsome.org/docs/client-api

import Vuetify from 'vuetify';
import 'vuetify/dist/vuetify.min.css';
import MainLayout from '@/layouts/MainLayout.vue';
import '@/assets/styles/main.scss';

export default function(Vue, { router, head, isClient, appOptions }) {
  head.link.push({
    rel: 'stylesheet',
    href: 'https://fonts.googleapis.com/icon?family=Material+Icons'
  });

  head.link.push({
    rel: 'stylesheet',
    href:
      'https://fonts.googleapis.com/css?family=Open+Sans:300,400|Roboto:500&display=swap'
  });

  //const opts = {...}; //opts includes, vuetify themes, icons, etc.
  Vue.use(Vuetify);

  appOptions.vuetify = new Vuetify();

  // Set default layout as a global component
  Vue.component('Layout', MainLayout);
}

// Firebase App (the core Firebase SDK) is always required and
// must be listed before other Firebase SDKs
import * as firebase from 'firebase/app';

// Add the Firebase services that you want to use
import 'firebase/auth';
import 'firebase/firestore';

var firebaseConfig = {
  apiKey: 'AIzaSyA9s3ee8_g90SHKVEljpmj9n5d1GdTM8WM',
  authDomain: 'api-project-538004955154.firebaseapp.com',
  databaseURL: 'https://api-project-538004955154.firebaseio.com',
  projectId: 'api-project-538004955154',
  storageBucket: 'api-project-538004955154.appspot.com',
  messagingSenderId: '538004955154',
  appId: '1:538004955154:web:57625f14f10bd9059278cb'
};

firebase.initializeApp(firebaseConfig);

let firebase_db = firebase.firestore();

// Initialize Firebase
export default firebase;
export const db = firebase_db;

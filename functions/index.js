const functions = require('firebase-functions');
const admin = require('firebase-admin');

const express = require('express');
const cors = require('cors');
const uuidv1 = require('uuid/v1');

const app = express();

admin.initializeApp();
const db = admin.firestore();

// Automatically allow cross-origin requests
app.use(cors({ origin: true }));
app.use(express.json()); // for parsing application/json
app.use(express.urlencoded({ extended: true })); // for parsing application/x-www-form-urlencoded

app.post('/', async (req, res) => {
  let password = req.body.password;
  if (!password) res.status(400).send('Missing password body');
  let uuid = uuidv1();
  // Get password collection
  try {
    let doc = await db
      .collection('password')
      .doc('password')
      .get();
    if (doc.exists) {
      let verify_password = doc.data().password;
      if (password === verify_password) {
        admin
          .auth()
          .createCustomToken(uuid)
          .then(customToken => {
            return res.send(customToken);
          })
          .catch(error => {
            console.log('Error creating custom token:', error);
            return res.send(error);
          });
      } else {
        return res.status(401).send('Wrong password');
      }
    } else {
      return res.status(400).send('Error verifying password');
    }
  } catch (error) {
    console.log('Error retrieving data', error);
    return res.send(error);
  }
  return res.send('Undefined');
});

exports.authenticate = functions.https.onRequest(app);
